# Git Directory

Git Directory is a simple global git hook that automatically adds a symlink to a repository to the GIT_REPOS environment variable 
defaulting to ~/git-repos, using the `post-checkout` hook. This is intended for both easier access to a user's various repos as well as 
making writing sctipts to search over all of a user's git repos, such as a rofi script to automatically open an Emacs session in a repo.
